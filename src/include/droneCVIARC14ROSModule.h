//////////////////////////////////////////////////////
//  DroneCVIARC14ROSModule.h
//
//  Created on: Jul 25, 2014
//      Author: joselusl
//
//  Last modification on: Jul 25, 2014
//      Author: joselusl
//
//////////////////////////////////////////////////////

#ifndef DRONE_CV_IARC_14_ROS_MODULE_H
#define DRONE_CV_IARC_14_ROS_MODULE_H




//I/O stream
//std::cout
#include <iostream>

//Vector
//std::vector
#include <vector>

//String
//std::string, std::getline()
#include <string>

//String stream
//std::istringstream
#include <sstream>

//File Stream
//std::ofstream, std::ifstream
#include <fstream>

//String stream
//std::istringstream
#include <sstream>


// ROS
#include "ros/ros.h"



//Drone module
#include "droneModuleROS.h"


//Drone CV
#include "IARC_2014_interface/IARC_interface_reduced.h"




//ROS Images
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/image_encodings.h>


//OpenCV
#include <opencv2/opencv.hpp>



//Keypoints: Messages out
#include <droneMsgsROS/vector2i.h>
#include <droneMsgsROS/vectorPoints2DInt.h>


//Ground Robots: Messages out
#include <droneMsgsROS/targetInImage.h>
#include <droneMsgsROS/vectorTargetsInImageStamped.h>







//#define CV_DEBUG


/////////////////////////////////////////
// Class DroneKeypointsGridDetectorROSModule
//
//   Description
//
/////////////////////////////////////////
class DroneKeypointsGridDetectorROSModule : public DroneModule
{	
protected:
    //exchange info
    std::vector<cv::Point> gridIntersections; //cv::Point = cv::Point2i

    //Main class
    IARC14_grid_node IA14G;



protected:
    std::string camCalibParamFile;
    std::string gridIntersectionsParamFile;



public:
    int setCalibCamera(std::string camCalibParamFile);
    int setTopicConfigs(std::string camSubsTopicName, std::string keypointsPublTopicName);
    int setGridParameters(std::string gridIntersectionsParamFile);


    //subscribers
protected:
    //Topic name
    std::string camSubsTopicName;
    //Front image msgs
    cv_bridge::CvImagePtr cvDroneImage;
    cv::Mat droneImage;
    //Subscriber
    ros::Subscriber droneImageSubs;
public:
    void droneImageCallback(const sensor_msgs::ImageConstPtr& msg);


    //publishers
protected:
    //Topic name
    std::string keypointsPublTopicName;
    ros::Publisher droneKeypointsPubl;
    bool publishKeypoints();




public:
    DroneKeypointsGridDetectorROSModule();
    ~DroneKeypointsGridDetectorROSModule();
	
public:
    void open(ros::NodeHandle & nIn);
	void close();

protected:
    bool init();

    //Reset
protected:
    bool resetValues();

    //Start
protected:
    bool startVal();

    //Stop
protected:
    bool stopVal();

    //Run
public:
    bool run();


};






/////////////////////////////////////////
// Class DroneGroundRobotsDetectorROSModule
//
//   Description
//
/////////////////////////////////////////
class DroneGroundRobotsDetectorROSModule : public DroneModule
{
protected:
    //exchange info
    std::vector<cv::Rect> groundRobotsROIs;
    std::vector<int> groundRobotsType;
    std::vector<int> groundRobotsId;

    //Main class
    IARC14_bot_node IA14B;


protected:
    std::string violaJonesModel;
    std::string svmModel;
    std::string svmScaleValues;


protected:
    std::string camCalibParamFile;
    std::string botParamFile;


public:
    int setCalibCamera(std::string camCalibParamFile);
    int setTopicConfigs(std::string camSubsTopicName, std::string groundRobotsPublTopicName);
    int setCVConfigs(std::string violaJonesModel, std::string svmModel, std::string svmScaleValues);
    int setBotParameters(std::string botParamFile);


    //subscribers
protected:
    //Topic name
    std::string camSubsTopicName;
    //Front image msgs
    cv_bridge::CvImagePtr cvDroneImage;
    cv::Mat droneImage;
    //Subscriber
    ros::Subscriber droneImageSubs;
public:
    void droneImageCallback(const sensor_msgs::ImageConstPtr& msg);


    //publishers
protected:
    //Topic name
    std::string groundRobotsPublTopicName;
    ros::Publisher droneGroundRobotsPubl;
    bool publishGroundRobots();




public:
    DroneGroundRobotsDetectorROSModule();
    ~DroneGroundRobotsDetectorROSModule();

public:
    void open(ros::NodeHandle & nIn);
    void close();

protected:
    bool init();

    //Reset
protected:
    bool resetValues();

    //Start
protected:
    bool startVal();

    //Stop
protected:
    bool stopVal();

    //Run
public:
    bool run();


};






#endif
