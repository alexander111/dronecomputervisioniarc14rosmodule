//////////////////////////////////////////////////////
//  DroneCVIARC14ROSModule_KeypointsNode.h
//
//  Created on: Jul 25, 2014
//      Author: joselusl
//
//  Last modification on: Jul 25, 2014
//      Author: joselusl
//
//////////////////////////////////////////////////////



//I/O Stream
//std::cout
#include <iostream>

#include <string>


// ROS
//ros::init(), ros::NodeHandle, ros::ok(), ros::spinOnce()
#include "ros/ros.h"



//cv module
#include "droneCVIARC14ROSModule.h"


//Nodes names
//MODULE_NAME_DRONE_KEYPOINTS_GRID_DETECTOR
#include "nodes_definition.h"





using namespace std;

int main(int argc, char **argv)
{
    //Init
    ros::init(argc, argv, MODULE_NAME_DRONE_KEYPOINTS_GRID_DETECTOR); //Say to ROS the name of the node and the parameters
    ros::NodeHandle n; //Este nodo admite argumentos!!

    std::string node_name=ros::this_node::getName();

    //Class
    DroneKeypointsGridDetectorROSModule MyDroneKeypointsGridDetectorROSModule;

    MyDroneKeypointsGridDetectorROSModule.open(n);


    //Loop -> Ashyncronous Module
    ros::spin();

    return 1;
}

